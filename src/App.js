import React, { Component } from 'react';
import FileViewer from 'react-file-viewer';
// import logger from 'logging-library';
import { CustomErrorComponent } from 'custom-error';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { file: '', imageUrl: '', isLoaded: false, documents: [], documentUrl: '', document: '', fileType: '' };
  }

  _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        imageUrl: reader.result
      });
    };

    reader.readAsDataURL(file);
  }
  getDocument(doc) {
    fetch(
      'https://peaceful-mesa-72076.herokuapp.com/download?id=' +
        doc.key.split('/')[0] +
        '&key=' +
        doc.key.split('/')[1],
      {
        method: 'GET',
        responseType: 'blob',
        headers: {
          //'Content-Type': 'application/json',
          Authorization:
            'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxOSIsImlhdCI6MTU0MzA3NDcwMiwiZXhwIjoxNTQzNjc5NTAyfQ.pCpR4xgu_r2qoxOlkychINKHEco9l5lqaNNQQ07X_p1G0I6O-s7uUlFQcDo9ltn_HteWy9pVeMUzl1lzjLLGvg'
        }
      }
    )
      .then(res => res.blob())
      .then(result => {
        console.log(result);
        var url = window.URL.createObjectURL(result);
        this.setState({
          isLoaded: true,
          document: result,
          documentUrl: url,
          fileType: doc.key.split('.')[1]
        });
      }, error => {
        // exceptions from actual bugs in components. // instead of a catch() block so that we don't swallow // Note: it's important to handle errors here
        this.setState({ isLoaded: true, error });
      });
  }
  _handleDownload(e, document) {
    this.getDocument(document);
  }

  _download(e) {
    var filename = 'document.' + this.state.fileType;
    var a = document.createElement('a');
    document.body.appendChild(a);
    var url = window.URL.createObjectURL(this.state.document);
    a.href = url;
    a.download = filename;
    a.click();
    setTimeout(() => {
      window.URL.revokeObjectURL(url);
      document.body.removeChild(a);
    }, 0);
  }

  componentDidMount() {
    fetch('https://peaceful-mesa-72076.herokuapp.com/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization:
          'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxOSIsImlhdCI6MTU0MzA3NDcwMiwiZXhwIjoxNTQzNjc5NTAyfQ.pCpR4xgu_r2qoxOlkychINKHEco9l5lqaNNQQ07X_p1G0I6O-s7uUlFQcDo9ltn_HteWy9pVeMUzl1lzjLLGvg'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          this.setState({ isLoaded: true, documents: result });
        },
        error => {
          // exceptions from actual bugs in components. // instead of a catch() block so that we don't swallow // Note: it's important to handle errors here
          this.setState({ isLoaded: true, error });
        }
      );
  }

  render() {
    var { documentUrl } = this.state;
    var { fileType } = this.state;
    var $download = null;
    var documents = this.state.documents;
    if (documentUrl) {
      $download = <a onClick={e => this._download(e)}>Click to download</a>;
    }

    return <div className="App">
        <input type="file" onChange={e => this._handleImageChange(e)} />
        {documents.map(document => <div key={document.key}>
            <a href="javascript:void(0)" onClick={e => this._handleDownload(e, document)}>
              {document.key}
            </a>
          </div>)}
        <div className="preview">
          {documentUrl ? (
            <FileViewer
              fileType={fileType}
              filePath={documentUrl}
              errorComponent={CustomErrorComponent}
              onError={this.onError}
            />
          ) : (
            ''
          )}
        </div>
        {$download}
      </div>;
  }
}

export default App;
